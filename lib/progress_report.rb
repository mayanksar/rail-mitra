class ProgressReport

  def initialize(project_id, date =  Date.today)
    @project = Project.find(project_id)
    @date = date
  end

  def progress_till_date
    
  end

  def dpr_by_date
    {
      milestones: milestones_data,
      bridges: bridges_data,
      other_activity: @project&.dpr_activities&.find_by(dpr_date: @date)&.description,
      dpr_machinary_use: dpr_machinary_use_data,
      dpr_labour_use: dpr_labour_use_data,
      other_activity: @project&.dpr_activities&.find_by(dpr_date: @date)
    }
  end

  private

  def milestones_data
    report = []

    @project.milestones.each do |milestone|
      dpr_milestone = milestone&.dpr_milestones&.find_by(dpr_date: @date)
      report << {
                    description: milestone&.description,
                    target: milestone&.target,
                    upto_percentage: milestone&.achieved_till_date(@date),
                    cummulative: milestone&.cummulative_till_date(@date),
                    balance: milestone&.balance_till_date(@date),
                    upto_date: milestone&.total_till_date(@date),
                    progress: dpr_milestone&.progress,
                    id: milestone&.id,
                    unit: milestone&.unit&.name,
                    activity: dpr_milestone&.activity
                }
    end
    report
  end

  def bridges_data
    report = []

    @project.bridges.each do |bridge|
      dpr_bridge = bridge.dpr_bridges.find_by(dpr_date: @date)
      report << { 
          title: bridge&.title,
          span: bridge&.span,
          excavation: dpr_bridge&.excavation,
          sand_soiling_and_pcc: dpr_bridge&.sand_soiling_and_pcc, 
          top_slab_and_parapet: dpr_bridge&.top_slab_and_parapet, 
          face_wall_bottom: dpr_bridge&.face_wall_bottom,
          id: bridge&.id,
          activity: dpr_bridge&.activity
        }
    end
    report
  end

  def dpr_machinary_use_data
    report = []
    dpr_machinary_uses = @project.dpr_machinary_uses.where(dpr_date: @date)
    dpr_machinary_uses.each do |dpr_machinary_use|
      report << {
        machine_name: dpr_machinary_use&.machinary&.name, 
        count: dpr_machinary_use&.count
      }
    end
    report
  end

  def dpr_labour_use_data
    report = []
    dpr_labour_use = @project&.dpr_labour_uses&.find_by(dpr_date: @date)
    {
      drain: dpr_labour_use&.drain, 
      central_wall: dpr_labour_use&.central_wall,
      earth_work_in_filling: dpr_labour_use&.earth_work_in_filling,
      blanketing: dpr_labour_use&.blanketing
    }
  end

end