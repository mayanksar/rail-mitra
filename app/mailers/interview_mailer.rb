class InterviewMailer < ApplicationMailer

  def interview_mail(interview)
    @interview = interview
    @profile = @interview.profile
    @lead = @interview.lead
    email =  Rails.env == 'production' ? @profile.email : 'TestS@mailinator.com'
    mail(:to => email,
         :subject => "Interview: schedule on #{@interview.schedule_date}")
  end
end
