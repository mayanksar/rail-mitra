class ProjectsController < ApplicationController

  before_action :authenticate_user!
  before_action :check_admin_permission, only: [:new, :create, :edit, :update, :assign, :new_assign ]

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)

    set_accessor_attr

    respond_to do |format|
      if @project.save
        format.html { redirect_to projects_path, notice: 'project created successfully.' }
      else
        format.html { render :new }
      end
    end
  end

  def edit
    @project = current_user.projects.find(params[:id])
  end

  def update
    @project = current_user.projects.find(params[:id])

    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to projects_path, notice: 'Project updated successfully.' }
      else
        format.html { render :edit }
      end
    end
  end

  def index
    @projects = current_user.projects
  end

  def show
    @project = current_user.projects.find(params[:id])
  end

  def new_assign
    @project = Project.find(params[:id])
  end

  def assign
    @project = Project.find(params[:id])

    unless assignment_params.blank?
      @project.users.delete_all
      @project.users << User.find(assignment_params[:users])
    end
    redirect_to projects_path, notice: 'Project assigned successfully.'
  end

  private

  def project_params
    params.require(:project).permit(:title, :agency, 
                                    :deadline, :begin_date,
                                    :contarct_agreement_number)
  end

  def set_accessor_attr
    @project.user_id = current_user.id
  end

  def assignment_params
    params.require(:assignments).permit(users:[])
  end
end