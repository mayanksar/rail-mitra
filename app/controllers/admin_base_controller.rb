class AdminBaseController < ApplicationController
  protect_from_forgery prepend: true

  protected

  def after_sign_in_path_for(resource)
    admins_path
  end
end
