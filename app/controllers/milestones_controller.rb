class MilestonesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin_permission

  def new
    project = Project.find(params[:project_id])
    @milestone = project.milestones.build
  end

  def create
    project = Project.find(params[:project_id])
    @milestone = project.milestones.build(milestone_params)

    respond_to do |format|
      if @milestone.save
        format.js { redirect_to project_path(project), notice: 'Milestone added successfully.' }
      else
        format.js { render :new }
      end
    end
  end

  def edit
    @milestone = Milestone.find(params[:id])
  end

  def update
    project = Project.find(params[:project_id])
    @milestone = project.milestones.find(params[:id])

    respond_to do |format|
      if @milestone.update(milestone_params)
        format.js { redirect_to project_path(project), notice: 'Milestone updated successfully.' }
      else
        format.js { render :edit }
      end
    end
  end

  def show
  end

  private

  def milestone_params
    params.require(:milestone).permit(:target, :description, :deadline, :unit_id)
  end
end
