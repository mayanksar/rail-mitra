class ApplicationController < ActionController::Base


  def check_admin_permission
    unless current_user.has_admin_permission?
      redirect_back fallback_location: root_path
    end
  end
  
  private

end
