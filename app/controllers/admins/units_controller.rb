class Admins::UnitsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin_permission

  def index
    @units = Unit.where(archieve: false).or(Unit.where(archieve: nil))
  end

  def new
    @unit = Unit.new
  end

  def create
    @unit = Unit.new(unit_params)

    respond_to do |format|
      if @unit.save
        format.html { redirect_to admins_unit_path(@unit), notice: 'Unit created successfully.' }
      else
        format.html { render :new }
      end
    end
  end

  def  edit
    @unit = Unit.find(params[:id])
  end

  def  update
    @unit = Unit.find(params[:id])

    respond_to do |format|

      if @unit.update(user_params)
        format.html { redirect_to admins_user_path(@unit), notice: 'user updated successfully.' }
      else
        format.html { render :edit }
      end
    end
  end

  def show
    @unit = Unit.find(params[:id])
  end

  def destroy
    @unit = Unit.find(params[:id])

    @unit.update(archieve: true)
    respond_to do |format|
      format.html { redirect_to admins_unit_path, notice: "Unit destroyed successfully." }
    end
  end

  private

  def unit_params
    params.require(:unit).permit(:name)
  end

end
