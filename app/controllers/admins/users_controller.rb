class Admins::UsersController < AdminBaseController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.skip_password_validation = true
    respond_to do |format|
      if @user.save
        format.html { redirect_to admins_user_path(@user), notice: 'user created successfully.' }
      else
        format.html { render :new }
      end
    end
  end

  def  edit
  end

  def  update
    respond_to do |format|
      @user.skip_password_validation = true
      if @user.update(user_params)
        format.html { redirect_to admins_user_path(@user), notice: 'user updated successfully.' }
      else
        format.html { render :new }
      end
    end
  end

  def show
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admins_users_path, notice: "User #{@user.full_name} destroyed successfully." }
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :mobile_no, :dob, :date_of_joining, :email, :role, :photo)
  end

  def set_user
    @user = User.find_by(id: params[:id])
  end
end
