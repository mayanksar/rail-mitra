class DprsController < ApplicationController
  before_action :authenticate_user!

  def new
    @project_dpr = current_user.projects.find(params[:project_id])

    @project_dpr.bridges.each do |bridge|
      bridge.dpr_bridges.build
    end

    @project_dpr.milestones.each do |milestone|
      milestone.dpr_milestones.build
    end

    Machinary.all.each do |machinary|
      @project_dpr.dpr_machinary_uses.build(:machinary_id => machinary.id) unless machinary.archieve
    end

    @project_dpr.dpr_labour_uses.build
    @project_dpr.dpr_activities.build
  end

  def create
    @project_dpr = current_user.projects.find(params[:project_id])

    @project_dpr.dpr_date = params[:report_date]

    respond_to do |format|
      if @project_dpr.dpr_submitted?(params[:date]) && @project_dpr.update(dpr_params)
        format.html { redirect_to projects_path, notice: 'dpr submitted successfully.' }
      else
        format.html { render :new, alert: 'something went wrong....' }
      end
    end
  end

  def edit
  end

  def update
  end

  def valid_dpr_date
    project = current_user.projects.find(params[:project_id])
    project.dpr_date = params[:date]
    
    respond_to do |format|

      unless project.dpr_submitted?(params[:date])         
        format.js { flash.now[:alert] =  "DPR already submitted for #{params[:date]}" }
      else
        format.js { render nothing: true, status: :ok }
      end
    end
  end

  def index
    @project = current_user.projects.find(params[:project_id])
    @dprs = @project.milestones.includes(:dpr_milestones).collect{|m| m.dpr_milestones.pluck(:dpr_date)}.flatten.uniq
  end

  def show
    @project = current_user.projects.find(params[:project_id])

    unless @project.dpr_submitted?(params[:date])
      @dpr_report = ProgressReport.new(@project.id, params[:date]).dpr_by_date
    else
      redirect_back(fallback_location: project_dprs_path, alert: 'DPR not submitted')
    end
  end

  private

  def dpr_params 
    params.require(:project).permit(
      milestones_attributes: [ 
        :id, 
        dpr_milestones_attributes: [ :progress, :activity ]
      ],
      bridges_attributes: [ 
        :id,
        dpr_bridges_attributes: [
                                  :excavation, 
                                  :sand_soiling_and_pcc,
                                  :top_slab_and_parapet,
                                  :face_wall_bottom,
                                  :activity,
                                  :labour_count
      ]],
      dpr_machinary_uses_attributes: [ :machinary_id, :count ],
      dpr_labour_uses_attributes: [
        :drain, :central_wall, :earth_work_in_filling, :blanketing
      ],
      dpr_activities_attributes: [ :description ]
    )
  end
end
