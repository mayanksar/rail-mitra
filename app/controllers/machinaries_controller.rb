class Admins::MachinariesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_admin_permission
  
  def index
    @machinaries = Machinary.where(archieve: false).or(Machinary.where(archieve: nil))
  end

  def new
    @machinary = Machinary.new
  end

  def create
    @machinary = Machinary.new(machinary_params)

    respond_to do |format|
      if @machinary.save
        format.html { redirect_to admins_machinaries_path, notice: 'Machinary created successfully.' }
      else
        format.html { render :new }
      end
    end
  end

  def  edit
    @machinary = Machinary.find(params[:id])
  end

  def  update
    @machinary = Machinary.find(params[:id])

    respond_to do |format|

      if @machinary.update(machinary_params)
        format.html { redirect_to admins_machinaries_path, notice: 'Machinary updated successfully.' }
      else
        format.html { render :edit }
      end
    end
  end

  def show
    @machinary = Machinary.find(params[:id])
  end

  def destroy
    @machinary = Machinary.find(params[:id])

    @machinary.update(archieve: true)
    respond_to do |format|
      format.html { redirect_to admins_machinary_path, notice: "Machinary destroyed successfully." }
    end
  end

  private

  def machinary_params
    params.require(:machinary).permit(:name)
  end

end
