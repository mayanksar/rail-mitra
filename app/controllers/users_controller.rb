class UsersController < BaseController

  def index
    @users = current_user.juniors
  end

  def show
    @user = current_user.juniors.find(params[:id])
  end
end
