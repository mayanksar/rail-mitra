class BridgesController < ApplicationController

  before_action :authenticate_user!
  before_action :check_admin_permission
  
  def new
    project = Project.find(params[:project_id])
    @bridge = project.bridges.build
  end


  def create
    project = Project.find(params[:project_id])
    @bridge = project.bridges.build(bridge_params)

    respond_to do |format|
      if @bridge.save
        format.js { redirect_to project_path(project), notice: 'Bridge added successfully.' }
      else
        format.js { render :new }
      end
    end
  end

  def edit
    @bridge = Bridge.find(params[:id])
  end

  def update
    project = Project.find(params[:project_id])
    @bridge = project.bridges.find(params[:id])

    respond_to do |format|
      if @bridge.update(bridge_params)
        format.js { redirect_to project_path(project), notice: 'Bridge updated successfully.' }
      else
        format.js { render :edit }
      end
    end
  end

  def show
  end

  private

  def bridge_params
    params.require(:bridge).permit(:title, :span)
  end

end
