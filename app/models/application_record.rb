class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.current_time
    Time.zone.now
  end
end
