class DprLabourUse < ApplicationRecord
  belongs_to :project


  before_create :set_dpr_date

  private

  def set_dpr_date
    self.dpr_date = self.project.dpr_date    
  end
end
