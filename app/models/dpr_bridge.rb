class DprBridge < ApplicationRecord
  belongs_to :bridge

  validates_numericality_of :excavation, less_than_or_equal_to: 100, greater_than: 0
  validates_numericality_of :sand_soiling_and_pcc, less_than_or_equal_to: 100, greater_than: 0
  validates_numericality_of :top_slab_and_parapet, less_than_or_equal_to: 100, greater_than: 0
  validates_numericality_of :face_wall_bottom, less_than_or_equal_to: 100, greater_than: 0

  private

  def set_dpr_date
    self.dpr_date = self.bridge.project.dpr_date    
  end

end
