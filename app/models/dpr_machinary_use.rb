class DprMachinaryUse < ApplicationRecord
  belongs_to :project
  belongs_to :machinary


  before_create :set_dpr_date

  private

  def set_dpr_date
    self.dpr_date = self.project.dpr_date    
  end
end
