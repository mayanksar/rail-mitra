class Bridge < ApplicationRecord
  belongs_to :project

  has_many :dpr_bridges, dependent: :destroy
  accepts_nested_attributes_for :dpr_bridges, allow_destroy: true


  def excavation_till_date(date = Date.today)
    dpr_bridges.where('dpr_date >= ?', date).order('dpr_date DESC')&.first&.excavation || 0
  end

  def sand_soiling_and_pcc_till_date(date = Date.today)
    dpr_bridges.where('dpr_date >= ?', date).order('dpr_date DESC')&.first&.sand_soiling_and_pcc || 0 
  end

  def top_slab_and_parapet_till_date(date = Date.today)
    dpr_bridges.where('dpr_date >= ?', date).order('dpr_date DESC')&.first&.top_slab_and_parapet || 0
  end

  def face_wall_bottom_till_date(date = Date.today)
    dpr_bridges.where('dpr_date >= ?', date).order('dpr_date DESC')&.first&.face_wall_bottom || 0
  end

end
