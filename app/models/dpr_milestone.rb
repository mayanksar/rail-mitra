class DprMilestone < ApplicationRecord
  belongs_to :milestone

  validates_numericality_of :progress, 
                            :greater_than_or_equal_to => 0,
                            :message => "My custom error message"
  validate :target_not_exceed

  before_create :set_dpr_date 
  

  private

  def set_dpr_date
    self.dpr_date = self.milestone.project.dpr_date    
  end

  def target_not_exceed
    if self.milestone.target <= self.milestone.dpr_milestones.sum(:progress) + progress
      errors.add(:progress, "sum can't be greater than target value")
      return false
    end
    true
  end
end
