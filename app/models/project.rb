class Project < ApplicationRecord
  has_and_belongs_to_many :users, -> { distinct }

  attr_accessor :user_id, :dpr_date

  before_create :permit_access

  has_many :milestones, dependent: :destroy
  accepts_nested_attributes_for :milestones, allow_destroy: true
  
  has_many :bridges, dependent: :destroy
  accepts_nested_attributes_for :bridges, allow_destroy: true

  has_many :dpr_machinary_uses, dependent: :destroy
  accepts_nested_attributes_for :dpr_machinary_uses, allow_destroy: true

  has_many :dpr_labour_uses, dependent: :destroy
  accepts_nested_attributes_for :dpr_labour_uses, allow_destroy: true

  has_many :dpr_activities, dependent: :destroy
  accepts_nested_attributes_for :dpr_activities, allow_destroy: true
    
  def dpr_submitted?(date)
    progress = milestones.joins(:dpr_milestones)
              .where('dpr_milestones.dpr_date = ?', date)
    
    progress.blank?
  end

  private

  def permit_access
    self.users << User.find(self.user_id)
  end
end
