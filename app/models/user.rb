class User < ApplicationRecord
  include EmployeeHierarchyModule
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable

  attr_accessor :role
  attr_accessor :created_by_admin
  attr_accessor :skip_password_validation


  devise :database_authenticatable, :trackable, #:confirmable,
         :recoverable, :rememberable, :validatable

  # Validations
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :dob, presence: true
  validates :mobile_no, presence: true,
            :length => { :minimum => 10, :maximum => 15 }


  has_and_belongs_to_many :projects

  def full_name
    "#{first_name&.humanize} #{last_name&.humanize}"
  end

  def role=(role)
    role = Role.find_by(name: role)
    add_role role.name
  end

  def role
    self.roles.map(&:name)
  end

  def has_admin_permission?
    has_role?(:Head) || has_role?(:CE) || has_role?(:DE)
  end

  protected

  def password_required?
    return false if skip_password_validation

    super
  end
end
