# frozen_string_literal: true

# Create new lead assignment on lead create/update
module LeadAssignmentModule
  extend ActiveSupport::Concern

  included do
    before_create :create_lead_assignment
    before_update :create_lead_assignment
  end

  def create_lead_assignment
  end
end
