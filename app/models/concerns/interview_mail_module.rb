module InterviewMailModule
  extend ActiveSupport::Concern

  module_function

  included do
    after_create :send_email
    after_update :send_email
  end

  def send_email
    MailService.interview_notification(self)
  end
end
