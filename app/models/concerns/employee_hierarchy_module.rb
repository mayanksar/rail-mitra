module EmployeeHierarchyModule
  extend ActiveSupport::Concern

  included do
    # Get lead
    has_one :employee_hierarchy, foreign_key: :user_id
    has_one :senior, -> { where('is_assigned = ? and unassigned_at is ?', true, nil) }, through: :employee_hierarchy

    # Get team member
    has_many :employee_hierarchies, foreign_key: :parent_id
    has_many :juniors, -> { where('is_assigned = ? and unassigned_at is ?', true, nil) }, through: :employee_hierarchies
  end
end
