module NotificationsModule
  extend ActiveSupport::Concern

  included do
    after_create :send_notification
    after_update :send_notification
  end

  private

  def send_notification
    notification = Notification.new(message: message,
                                    sender: user.id,
                                    reciever: user&.senior&.id)
    if notification.save
      ActionCable.server.broadcast 'notifications_channel',
                                   message: notification.message,
                                   content: { id: notification.reciever }
    end
  end

  def message
    case self.class.name
    when 'Profile'
      "<p><b>#{self.class.name}</b> #{full_name} #{created_or_updated} by the employee <b>#{user.full_name}</b></p>"
    when 'Lead'
      "<p><b>#{self.class.name}</b> #{full_name} #{created_or_updated} by the employee <b>#{user.full_name}</b></p>"
    when 'Interview'
      "<p><b>#{self.class.name}</b> #{created_or_updated} by the employee <b>#{user.full_name}</b> Assigned to #{interviewee&.full_name}</p>"
    when 'User'
      "<p><b>#{self.class.name}</b> #{created_or_updated} by the employee <b>#{user.full_name}</b></p>"
    when ''
      ''
    end
  end

  def created_or_updated
    updated_at == created_at ? 'Created' : 'Updated'
  end
end
