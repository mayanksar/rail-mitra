module ProfileDesignationModule
  extend ActiveSupport::Concern

  DESIGNATIONS = %w{ror django react angular node react_ror angular_ror react_django angular_django react_node angular_node designer}

  included do
    before_create :add_designation

    def add_designation
      DESIGNATIONS.collect do |designation|
        self.designations.build(title: designation.titleize, category: designation)
      end
    end
  end
end
