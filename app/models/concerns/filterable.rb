module Filterable
  extend ActiveSupport::Concern

  module ClassMethods
    def filter_by_params(filtering_params)
      results = self.where(nil)
      filtering_params.each do |key, value|
        results = results.where(key => value) if value.present?
      end
      results
    end
  end
end