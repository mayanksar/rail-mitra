class Milestone < ApplicationRecord

  belongs_to :project

  belongs_to :unit
  
  has_many   :dpr_milestones, dependent: :destroy
  accepts_nested_attributes_for :dpr_milestones, allow_destroy: true

  validates :target, presence: true
  validates :description, presence: true
  validates :unit_id, presence: true

  def achieved_till_date(date = Date.today)
    total_till_date(date)*100/target    
  end

  def cummulative_till_date(date = Date.today)
    0
  end

  def total_till_date(date = Date.today)
    dpr_milestones.where('dpr_date <= ?', date ).sum(:progress)
  end

  def balance_till_date(date = Date.today)
    target - total_till_date(date)
  end
end
