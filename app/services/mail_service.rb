class MailService

  def self.interview_notification(interview)
    InterviewMailer.interview_mail(interview).deliver
  end
end
