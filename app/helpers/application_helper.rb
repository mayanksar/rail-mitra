module ApplicationHelper
  def format_date(date)
    date&.strftime('%d-%m-%Y')
  end

  def current_action
    params[:action]
  end

  def new_action?
    ['new', 'create'].include? params[:action]
  end

  def associated_profiles_option(name)
    unless name.blank?
      Profile.find_by_user(name).pluck(:first_name, :ptype, :id).collect {|e| [e[0]+'-'+e[1], e[2]] }
    else
      Profile.pluck(:first_name, :ptype, :id).collect {|e| [e[0]+'-'+e[1], e[2]] }
    end
  end
end
