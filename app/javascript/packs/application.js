// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import 'jquery'
import 'bootstrap'
import 'packs/leads.js'

global.$ = require('jquery')

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")

require("trix")
require("@rails/actiontext")


$(document).on('click', '.active-tab', function(){
  $('.tab-button').removeClass('active-tab')
  $(this).addClass('active-tab')
});