document.addEventListener("turbolinks:load", function() {
  $('#lead_profile_id').change(function() {
    let profile_id = this.value;
    let url = this.dataset.url.replace(':profile_id', profile_id)
    let select = $('#lead_designation_id');
    if(profile_id !== ""){
      $.ajax({
        type:"GET",
        url : url,
        dataType : "json",
        async: false,
        success : function(response) {
          select.empty();
          $.each(response, function(index, value) {
            select.append(
              $('<option></option>').val(value.id).html(value.title)
            );
          });
          select.show();
        },
        error: function() {
          select.empty();
          console.log('Something is wrong!');
        }
      });
    }else{
      select.empty();
      console.log('Id not present!')
    }
  });
});
