import consumer from "./consumer"

consumer.subscriptions.create("NotificationsChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    $('#notification-area-'+data.content.id).append(data.message)
    $('#notifications-area').append(data.message)

    let count = parseInt($('.notifications-count').text())
    $('.notifications-count').text(count+1)
  }
});
