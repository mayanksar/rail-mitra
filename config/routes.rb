Rails.application.routes.draw do
  devise_for :users

  mount ActionCable.server, at: '/cable'

  root 'projects#index'

  namespace :admins do
    resources :users
    resources :units
    resources :machinaries
  end

  resources :projects do
    member do
      get :new_assign
      post :assign
    end

    resources :milestones
    resources :bridges
    resources :dprs do 
      collection do 
        get :valid_dpr_date

        match "/(:date)/by_date" => "dprs#show", 
            :constraints => { :date => /\d{4}-\d{2}-\d{2}/ },
            :as => "by_date", via: :get

      end
    end
  end


end
