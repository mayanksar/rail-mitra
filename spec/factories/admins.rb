FactoryBot.define do
  factory :admin do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email      { Faker::Internet.email }
    date_of_birth { Faker::Date.birthday(18, 40) }
    password   { Faker::Internet.password(6) }
    mobile_no  { Faker::PhoneNumber.cell_phone }
  end
end
