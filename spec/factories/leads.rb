FactoryBot.define do
  factory :lead do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    skype {  Faker::Internet.username(8) }
    zoom {  Faker::Internet.username(8) }
    jd_link { '' }
    platform { 'Test' }
    portal { 'upwork' }
    association :user, factory: :user
    association :profile, factory: :profile
    association :designation, factory: :designation
  end
end
