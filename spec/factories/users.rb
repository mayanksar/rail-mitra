FactoryBot.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name  { Faker::Name.last_name }
    email      { Faker::Internet.email }
    dob        { Faker::Date.birthday(18, 40) }
    date_of_joining { Faker::Date.birthday }
    password   { Faker::Internet.password(6) }
    mobile_no  { Faker::PhoneNumber.cell_phone }
  end

  trait :fresher_user do
    role {'Fresher'}
  end

  trait :bde_user do
    role {'BDE'}
  end

end
