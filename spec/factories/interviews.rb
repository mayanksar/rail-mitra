FactoryBot.define do
  factory :interview do
    datetime { Faker::Date.forward }
    contact_info { Faker::Internet.email }
    association :lead, factory: :lead
    association :user, factory: :user
    association :profile, factory: :profile
    association :interviewee, factory: :admin
  end
end
