FactoryBot.define do
  factory :attachment do
    cover_letter { Faker::Lorem.sentence }
    resume { UploadHelper.pdf }
    association :designation, factory: :designation
  end

  trait :with_resume do
    resume { UploadHelper.pdf }
  end

  trait :with_cover_letter do
    cover_letter { UploadHelper.pdf }
  end
end
