FactoryBot.define do
  factory :employee_hierarchy do
    user_id       {  }
    parent_id     {  }
    is_assigned   { true }
    unassigned_at { nil }
  end
end
