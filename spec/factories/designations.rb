FactoryBot.define do
  factory :designation do
    title { Faker::Lorem.sentence(3)  }
    category { Faker::Lorem.sentence(1) }
    association :profile, factory: :profile
  end
end
