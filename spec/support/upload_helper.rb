module UploadHelper
  extend self
  extend ActionDispatch::TestProcess

  def png_name; 'test-image-3.png' end
  def png; upload(png_name, 'image/png') end

  def jpg_name; 'test-image-2.jpeg' end
  def jpg; upload(jpg_name, 'image/jpg') end

  # def tiff_name; 'test-image.tiff' end
  # def tiff; upload(tiff_name, 'image/tiff') end

  def pdf_name; 'test.pdf' end
  def pdf; upload(pdf_name, 'application/pdf') end

  def txt_name; 'test.txt' end
  def pdf; upload(txt_name, 'application/text') end
  private

  def upload(name, type)
    file_path = Rails.root.join('spec', 'support', 'assets', name)
    fixture_file_upload(file_path, type)
  end
end
