require 'rails_helper'

feature 'Login' do

  scenario 'visit root page' do
    visit '/'
    click_link 'Login'
    expect(page).to have_content('Log in')
    expect(page).to have_content('Email')
    expect(page).to have_content('Password')
  end
end
