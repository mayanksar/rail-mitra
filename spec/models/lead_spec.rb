require 'rails_helper'
require 'pry'
RSpec.describe Lead, :type => :model do
  let(:lead) { FactoryBot.create(:lead) }

  describe "Associations" do
    it { should belong_to(:user) }
    it { should belong_to(:profile) }
    it { should belong_to(:designation) }

    it { should have_many(:notes) }
    it { should have_many(:reminders) }
    it { should have_many(:interviews) }
    it { should have_many(:discussions) }
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(lead).to be_valid
    end

    it "is not valid without a first_name" do
      lead.first_name = nil
      expect(lead).to_not be_valid
    end

    it "is not valid without last_name" do
      lead.last_name = nil
      expect(lead).to_not be_valid
    end

    it "is not valid without email" do
      lead.email = nil
      expect(lead).to_not be_valid
    end

    it "is not valid without platform" do
      lead.platform = nil
      expect(lead).to_not be_valid
    end

    it "is not valid without portal" do
      lead.portal = nil
      expect(lead).to_not be_valid
    end
  end

  describe '#full_name' do
    context 'full name' do
      it 'should have full name' do
        lead = FactoryBot.create(:lead)
        expect(lead.full_name).to eq "#{lead.first_name} #{lead.last_name}"
      end
    end
  end
end

