require 'rails_helper'

RSpec.describe LeadAssignment do
  let(:lead) { create(:lead) }
  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:lead_assignment) do
    create(:lead_assignment,
           lead_id: lead.id,
           user_id: user.id,
           assignee_id: admin.id,
           assignee_type: admin.class.name)
  end

  describe 'Associations' do
    it { should belong_to(:lead) }
    it { should belong_to(:user) }
  end

  describe 'Assign on lead create' do
    it { expect(lead.lead_assignments.present?).to eq(true) }
  end

  describe '#assignee_name' do
    it { expect(lead_assignment.assignee_name.present?).to eq(true) }
  end
end
