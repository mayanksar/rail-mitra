require 'rails_helper'

RSpec.describe EmployeeHierarchy do
  let(:employee_hierarchy) { FactoryBot.create(:employee_hierarchy) }

  describe 'Associations' do
    it { should belong_to(:senior) }
    it { should belong_to(:junior) }
  end
end
