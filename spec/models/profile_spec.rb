require 'rails_helper'
require 'pry'

RSpec.describe Profile, :type => :model do
  let(:profile) { FactoryBot.create(:profile) }

  describe "Associations" do
    it { should belong_to(:user) }

    it { should have_many(:interviews) }
    it { should have_many(:designations) }
    it { should have_many(:discussions) }
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(profile).to be_valid
    end

    it "is not valid without a first_name" do
      profile.first_name = nil
      expect(profile).to_not be_valid
    end

    it "is not valid without last_name" do
      profile.last_name = nil
      expect(profile).to_not be_valid
    end

    it "is not valid without email" do
      profile.email = nil
      expect(profile).to_not be_valid
    end

    it "is not valid without github" do
      profile.github = nil
      expect(profile).to_not be_valid
    end
  end

  describe 'Designations' do
    it "have designations" do
      expect((profile.designations.map(&:category) & Profile::DESIGNATIONS).empty?).to eq false
    end
  end

  describe '#full_name' do
    context 'full name' do
      it 'should have full name' do
        profile = FactoryBot.create(:profile)
        expect(profile.full_name).to eq "#{profile.first_name} #{profile.last_name}"
      end
    end
  end
end

