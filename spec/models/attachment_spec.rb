require 'rails_helper'
require 'pry'

RSpec.describe Attachment, :type => :model do
  let(:attachment) { FactoryBot.create(:attachment) }

  describe 'Associations' do
    it { should belong_to(:designation) }
  end

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(attachment).to be_valid
      expect(attachment.resume).to be_attached
    end
  end
end
