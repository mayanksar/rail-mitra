require 'rails_helper'

RSpec.describe User do
  let(:user) { create(:user) }

  describe 'Associations' do
    it { should have_many(:leads) }
    it { should have_many(:notes) }
    it { should have_many(:profiles) }
    it { should have_many(:interviews) }
    it { should have_many(:designations) }
  end

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(user).to be_valid
    end

    it 'is not valid without a first_name' do
      user.first_name = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without last_name' do
      user.last_name = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without email' do
      user.email = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without mobile number' do
      user.mobile_no = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without date of birth' do
      user.dob = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without date of joining' do
      user.date_of_joining = nil
      expect(user).to_not be_valid
    end
  end

  describe '#full_name' do
    context 'full name' do
      it 'should have full name' do
        user = FactoryBot.create(:user)
        expect(user.full_name).to eq("#{user.first_name} #{user.last_name}")
      end
    end
  end

  describe '#junior?' do
    it 'should have senior' do
      emp1 = FactoryBot.create(:user, :fresher_user)
      emp2 = FactoryBot.create(:user, :bde_user)
      employee_hierarchy = FactoryBot.build(:employee_hierarchy)
      employee_hierarchy.parent_id = emp2.id
      employee_hierarchy.user_id = emp1.id
      employee_hierarchy.save

      expect(emp1.junior?).to eq(true)
    end
  end

  describe '#senior?' do
    it 'should have juniors' do
      emp1 = FactoryBot.create(:user, :fresher_user)
      emp2 = FactoryBot.create(:user, :bde_user)
      employee_hierarchy = FactoryBot.build(:employee_hierarchy)
      employee_hierarchy.parent_id = emp2.id
      employee_hierarchy.user_id = emp1.id
      employee_hierarchy.save

      expect(emp1.senior).to eq(emp2)
      expect(emp2.senior?).to eq(true)
    end
  end

  describe '#employee_with_parent_roles_for?' do
    it 'should have juniors' do
      emp1 = FactoryBot.create(:user, :fresher_user)
      emp2 = FactoryBot.create(:user, :bde_user)
      seniors = emp1.employee_with_parent_roles
      expect(seniors.flatten).to include(emp2.full_name)
    end
  end
end
