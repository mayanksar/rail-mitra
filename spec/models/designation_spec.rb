require 'rails_helper'
require 'pry'

RSpec.describe Designation, :type => :model do
  let(:designation) { FactoryBot.create(:designation) }

  describe "Associations" do
    it { should belong_to(:profile) }
    it { should have_many(:leads) }
    it { should have_many(:attachments) }
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(designation).to be_valid
    end

    it "is not valid without a title" do
      designation.title = nil
      expect(designation).to_not be_valid
    end

    it "is not valid without category" do
      designation.category = nil
      expect(designation).to_not be_valid
    end
  end
end

