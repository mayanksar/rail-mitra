require 'rails_helper'

RSpec.describe InterviewAssignHistory do
  let(:interview) { create(:interview) }
  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:interview_assign_history) do
    create(:interview_assign_history,
           interview_id: interview.id,
           user_id: user.id,
           assignee_id: admin.id,
           assignee_type: admin.class.name)
  end

  describe 'Associations' do
    it { should belong_to(:user) }
    it { should belong_to(:interview) }
  end

  describe 'Assign on interview create' do
    it { expect(interview.interview_assign_histories.present?).to eq(true) }
  end

  describe '#assignee_name' do
    it { expect(interview_assign_history.assignee_name.present?).to eq(true) }
  end
end
