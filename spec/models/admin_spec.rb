require 'rails_helper'
require 'pry'
RSpec.describe Admin, :type => :model do
  let(:admin) { FactoryBot.create(:admin) }

  describe "Associations" do
    it { should have_many(:users) }
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(admin).to be_valid
    end

    it "is not valid without a first_name" do
      admin.first_name = nil
      expect(admin).to_not be_valid
    end

    it "is not valid without last_name" do
      admin.last_name = nil
      expect(admin).to_not be_valid
    end

    it "is not valid without email" do
      admin.email = nil
      expect(admin).to_not be_valid
    end

    it "is not valid without mobile number" do
      admin.mobile_no = nil
      expect(admin).to_not be_valid
    end

    it "is not valid without date of birth" do
      admin.date_of_birth = nil
      expect(admin).to_not be_valid
    end
  end

  describe '#full_name' do
    context 'full name' do
      it 'should have full name' do
        admin = FactoryBot.create(:admin)
        expect(admin.full_name).to eq "#{admin.first_name} #{admin.last_name}"
      end
    end
  end

  describe '.archived' do
    context 'archived admin' do
      it 'list archived admin' do
        admin = FactoryBot.build(:admin)
        admin.archived_at = Time.now
        admin.save

        expect(Admin.archived).to include admin
      end
    end
  end
end

