require 'rails_helper'
require 'pry'

RSpec.describe Interview, :type => :model do
  let(:interview) { FactoryBot.create(:interview) }

  describe "Associations" do
    it { should belong_to(:user) }
    it { should belong_to(:lead) }
    it { should belong_to(:profile) }
    it { should belong_to(:interviewee) }
  end

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(interview).to be_valid
    end

    it "is not valid without a datetime" do
      interview.datetime = nil
      expect(interview).to_not be_valid
    end

    it "is not valid without contact info" do
      interview.contact_info = nil
      expect(interview).to_not be_valid
    end

    it "is not valid without profile" do
      interview.profile_id = nil
      expect(interview).to_not be_valid
    end

    it "is not valid without user" do
      interview.user_id = nil
      expect(interview).to_not be_valid
    end

    it "is not valid without lead" do
      interview.lead_id = nil
      expect(interview).to_not be_valid
    end
  end

  describe '#schedule_date' do
    context 'scheduled at' do
      it 'should have schedule date in format' do
        expect(interview.schedule_date).to eq interview.datetime&.strftime('%d/%m/%Y %H:%M %p')
      end
    end
  end

  describe '.send_notification' do
    context 'send email when interview create' do
      it 'send email notification' do
        interview = FactoryBot.create(:interview)
        expect(ActionMailer::Base.deliveries.last&.subject).to eq("Interview: schedule on #{interview.schedule_date}")
      end
    end
  end

end

