class CreateDprMilestones < ActiveRecord::Migration[6.0]
  def change
    create_table :dpr_milestones do |t|
      t.date    :progress_date
      t.float   :progress
      t.integer :milestone_id
      t.integer :project_id
      t.text    :activity

      t.timestamps
    end
  end
end
