class CreateMilestones < ActiveRecord::Migration[6.0]
  def change
    create_table :milestones do |t|
      t.float     :target
      t.string    :description
      t.date      :deadline
      t.integer   :unit_id
      t.integer   :project_id

      t.timestamps
    end
  end
end
