class CreateDprMachinaryUses < ActiveRecord::Migration[6.0]
  def change
    create_table :dpr_machinary_uses do |t|
      t.integer :count
      t.integer :machinary_id
      t.integer :project_id
      t.date    :dpr_date

      t.timestamps
    end
  end
end
