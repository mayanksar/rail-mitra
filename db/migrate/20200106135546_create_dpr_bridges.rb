class CreateDprBridges < ActiveRecord::Migration[6.0]
  def change
    create_table :dpr_bridges do |t|
      t.float :excavation
      t.float :sand_soiling_and_pcc
      t.float :top_slab_and_parapet
      t.float :face_wall_bottom
      t.text  :activity
      t.integer :bridge_id
      t.integer :labour_count

      t.timestamps
    end
  end
end
