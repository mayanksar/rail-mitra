class CreateDprActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :dpr_activities do |t|
      t.integer :project_id
      t.date :dpr_date
      t.text :description
      
      t.timestamps
    end
  end
end
