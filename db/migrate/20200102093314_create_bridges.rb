class CreateBridges < ActiveRecord::Migration[6.0]
  def change
    create_table :bridges do |t|
      t.string  :title
      t.float   :span
      t.integer :project_id

      t.timestamps
    end
  end
end
