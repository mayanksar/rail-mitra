class AddDprDateToDprMilestones < ActiveRecord::Migration[6.0]
  def change
    add_column :dpr_milestones, :dpr_date, :date
    remove_column :dpr_milestones, :progress_date
  end
end
