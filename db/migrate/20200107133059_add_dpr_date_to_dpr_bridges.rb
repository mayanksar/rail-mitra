class AddDprDateToDprBridges < ActiveRecord::Migration[6.0]
  def change
    add_column :dpr_bridges, :dpr_date, :date
  end
end
