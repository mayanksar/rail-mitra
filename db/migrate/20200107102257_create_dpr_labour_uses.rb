class CreateDprLabourUses < ActiveRecord::Migration[6.0]
  def change
    create_table :dpr_labour_uses do |t|
      t.integer :project_id
      t.date    :dpr_date
      t.integer :drain
      t.integer :central_wall
      t.integer :earth_work_in_filling
      t.integer :blanketing

      t.timestamps
    end
  end
end
