# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if ['development', 'test'].include? Rails.env
  ROLES = ['Head', 'CE', 'DCE', 'EE', 'AE', 'JE']
  ROLES.each_with_index{|role, i| Role.create!(name: role)}
end

if ['development'].include? Rails.env
  # User create
  user = User.create!(first_name: 'Mayank', last_name: 'Saraf', mobile_no: '9999999999',  dob: '01/09/1989', password: '123456', password_confirmation: '123456', email: 'msaraf@mailinator.com')
  puts user
end


if ['development'].include? Rails.env
  ['km', 'cum', 'each', 'rmt'].each { |unit|
    Unit.create(name: unit)
  }
end


